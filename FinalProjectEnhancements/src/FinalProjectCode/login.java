/************************************************************************
 * Project: CS 405: Secure Coding Final Project/Utility Company System  *
 * Created: Nov. 5, 2017												*
 * Updated: 															*
 * 																		*
 * Original Author: Curtis												*
 * Updating Author: Christian Blosser									*
 * Description:															*
 * 		This program is a theoretical program that could be used at a	*
 * 		water treatment facility that supplies water to residents in	*
 * 		town.  It encompasses bill creation, bill calculation, and CRUD	*
 * 		operations for the billing system.  This program was originally	*
 * 		written in C++.  The following is program is Java.	For the most*
 * 		part, it was a line for line translation.  A few adjustments	*
 * 		were made due to some special requirements need for the final.	*
 ************************************************************************/

/*
 * File: 	login.java
 * Created:	Nov. 5, 2017
 * Updated: Sep. 20, 2020
 * 
 * Author: 	Curtis
 * Updater: Christian Blosser
 * 
 * Description:
 * 		login.java controls the admin authentication process.  There are four
 * 		methods enclosed within that handle the process.  First the user enters
 * 		the username and password.  Then, a CSV file is read that contains a
 * 		list of usernames, passwords, first names, and last names.  Each of the
 * 		usernames and passwords are checked against the user input and handled
 * 		appropriately.
 * 
 * 		The style in how to handle the authentication process has changed
 * 		slightly from the original version written by Curtis.  Since there was
 * 		no access to the original input.txt file, an additional file and
 * 		respective implementations were created.
 */

package FinalProjectCode;

import java.io.File;		//import file handling class
import java.util.Scanner;	//import scanner to read file

public class login 
{
	//creation of needed instance variables
	private static String password;		//user entered password
	private static String username;		//user entered username
	private static char[] passwordTest;	//test array to compare against
	private static char[] usernameTest;	//test array to compare against
	private static char[] passwordArray;//string to character array for user entry
	private static char[] usernameArray;//string to character array for user entry
	
	//boolean instance variable to allow authentication or deny it
	private static boolean PasswordStatus;
	
	//enterPassword() handles everything needed for user to enter password
	public static void enterPassword()
	{
		//prompt user to enter password
		System.out.println("Enter Password");
		//call created String input method in runner
		login.password = final_utility_company_system.userString();
		//translate user input to array to compare against
		login.passwordArray = password.toCharArray();
	}
	
	//enterUsername() handles everything needed for user to enter username
	public static void enterUsername()
	{
		//prompt user to enter username
		System.out.println("Enter Username");
		//call created String input method in runner
		login.username = final_utility_company_system.userString();
		//translate user input to array to compare against
		login.usernameArray = username.toCharArray();
	}
	
	//testUserAndPass() handles authentication testing.  It uses instance arrays
	//in order to compare character by character if the entered credentials match
	//the database of usernames and passwords
	//@return boolean to let the class know the authentication was successful
	private static boolean testUserAndPass() throws Exception
	{
		PasswordStatus = false;	//initialize to fail under any circumstance
		int i = 0;	//variable to step through arrays
		//variables to compare user input and file input length
		int userLenEntr = usernameArray.length;
		int userLenTest = usernameTest.length;
		int passLenEntr = passwordArray.length;
		int passLenTest = passwordArray.length;
		
		//if all arrays are of same length, each has then be populated
		if ((userLenEntr == userLenTest) && (passLenEntr == passLenTest))
		{
			//loop through each username array to verify integrity of user entries
			for(i = 0; i < login.usernameArray.length; i++)
			{
				//each index of each username array is tested against each other
				if(login.usernameArray[i] == login.usernameTest[i])
				{
					//update boolean if the characters match
					login.PasswordStatus = true;
				}else	//otherwise
				{
					//update boolean as false  if comparison fails
					login.PasswordStatus = false;
					//print prompt to indicate wrong password
					System.out.println("Password failure");
					break;
				}
			}
			
			//loop through each password array to verify integriyt of user entries
			for(i = 0; i < login.passwordArray.length; i++)
			{
				//each index of each password array is tested against each other
				if(login.passwordArray[i] == login.passwordTest[i])
				{
					//update boolean if the characters match
					login.PasswordStatus = true;
				}else	//otherwise
				{
					//update boolean as false if comparison fails
					login.PasswordStatus = false;
					//print prompt to indicate wrong username
					System.out.println("Username failure");
					break;
				}
			}
			
			System.out.println("Login Success!");
		}else	//otherwise
		{
			//prompt failure with authentication and retry entry
			System.out.println("Problem with username or password, try again");
			loginRequest();
		}
		
		//return true or false to continue authentication process
		return login.PasswordStatus;
	}
	
	//readCSVForTest() is responsible for reading contents of a CSV file and
	//assigning those contents to their respective variables.  Since the CSV
	//file is of certain format, each entry is read accordingly
	private static void readCSVForTest() throws Exception
	{
		//flag and flag counter for placeholder while finding records
		int flag = 0;
		int counter = 0;
		
		//temp Strings to hold possible usernames and passwords
		String passTemp = "";
		String userTemp = "";
		
		//create scanner object to read file from project files
		Scanner readCSV = new Scanner(new File("loginFile.csv"));
		readCSV.useDelimiter(",");	//read comma to separate columns
		
		//loop through file to find username and password
		while (readCSV.hasNext())	//while more exists
		{
			counter++;	//add to counter, starts at 1 for flag
			String temp = readCSV.next();	//set temp for next()
			
			//if username in file matches user entered username
			if(temp.equals(login.username))
			{
				flag = counter;		//flag the new result/username
				userTemp = temp;	//store the new result/username
				passTemp = readCSV.next();	//store next entry/password
				//if flag is not 0 AND the next entry matches the password
				if(flag != 0 && passTemp.equals(login.password))
				{
					break;	//break from loop
				}else
				{
					//reset local variables to try again
					flag = 0;
					userTemp = "";
					passTemp = "";
				}
			}
		}
		
		//translate new found items into proper testing arrays for comparison
		login.passwordTest = passTemp.toCharArray();
		login.usernameTest = userTemp.toCharArray();
		
		//close scanner object
		readCSV.close();
	}
	
	//loginRequest() is responsible to handle the other methods of this class
	//and perform each action in correct order.
	//@return boolean for authentication check in runner
	public static boolean loginRequest() throws Exception
	{
		enterUsername();
		enterPassword();
		readCSVForTest();
		return testUserAndPass();
	}
}
