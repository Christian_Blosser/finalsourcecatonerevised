/************************************************************************
 * Project: CS 405: Secure Coding Final Project/Utility Company System  *
 * Created: Nov. 5, 2017												*
 * Updated: 															*
 * 																		*
 * Original Author: Curtis												*
 * Updating Author: Christian Blosser									*
 * Description:															*
 * 		This program is a theoretical program that could be used at a	*
 * 		water treatment facility that supplies water to residents in	*
 * 		town.  It encompasses bill creation, bill calculation, and CRUD	*
 * 		operations for the billing system.  This program was originally	*
 * 		written in C++.  The following is program is Java.	For the most*
 * 		part, it was a line for line translation.  A few adjustments	*
 * 		were made due to some special requirements need for the final.	*
 ************************************************************************/

/*
 * File: 	service.java
 * Created:	Nov. 5, 2017
 * Updated: Sep. 20, 2020
 * 
 * Author: 	Curtis
 * Updater: Christian Blosser
 * 
 * Description:
 * 		service.java only displays prompts for the user to enter a new
 * 		service.  Other methods are simple getters for future expansion.
 */

package FinalProjectCode;

public class service
{
	static int customerNumber;
	int serviceNumber;
	static int discount;
	
	static double servicePrice;
	double quantity;
	double tax;
	
	static String name;
	
	public service()
	{
		
	}
	
	public static void createService()
	{
		System.out.println("Please Enter The Service Number");
		customerNumber = final_utility_company_system.userInteger();
		System.out.println("Please Enter The Name of The Service");
		name = final_utility_company_system.userString();
		System.out.println("Please Enter The Price of The Service");
		servicePrice = final_utility_company_system.userDouble();
		System.out.println("Please Enter The Discount (%)");
		discount = final_utility_company_system.userInteger();
	}
	
	
	public static int getCusNum()
	{
		return customerNumber;
	}
	
	public static String getServName()
	{
		return name;
	}
	
	public static double getServPrice()
	{
		return servicePrice;
	}
	
	public static double getDiscount()
	{
		return discount;
	}
}
