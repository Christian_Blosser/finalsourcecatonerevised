/************************************************************************
 * Project: CS 405: Secure Coding Final Project/Utility Company System  *
 * Created: Nov. 5, 2017												*
 * Updated: 															*
 * 																		*
 * Original Author: Curtis												*
 * Updating Author: Christian Blosser									*
 * Description:															*
 * 		This program is a theoretical program that could be used at a	*
 * 		water treatment facility that supplies water to residents in	*
 * 		town.  It encompasses bill creation, bill calculation, and CRUD	*
 * 		operations for the billing system.  This program was originally	*
 * 		written in C++.  The following is program is Java.	For the most*
 * 		part, it was a line for line translation.  A few adjustments	*
 * 		were made due to some special requirements need for the final.	*
 ************************************************************************/

/*
 * File: 	gas.java
 * Created:	Nov. 5, 2017
 * Updated: Sep. 20, 2020
 * 
 * Author: 	Curtis
 * Updater: Christian Blosser
 * 
 * Description:
 * 		gas.java is a simple java file that calculates a total water bill based on the
 * 		number of gallons used.  It contains two functions, one uses the other.  The
 * 		constructor is needed for external calls to the functions within.
 */

package FinalProjectCode;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class gas 
{	
	//Basic constructor method.  No values needed to input
	//needed for future upgrades and calling the class
	public gas()
	{
		//no variables need passed through constructor
	}
	
	//gasFeeCalculation() acquires user input and calculates a bill based
	//on user input.  It then adds a $15 fee and displays gallons used and bill
	public static void gasFeeCalculation()
	{
		//DecimalFormat used to output bill in $xx.xx format
		DecimalFormat df = new DecimalFormat("#.##");
		//Round up to the nearest cent
		df.setRoundingMode(RoundingMode.CEILING);

		//initialize and declare all variables to be used in
		//gas fee calculation
		double gallons = 0;	//user input variable
		double charge = 0;	//charge before total
		double total = 0;	//used to calculate total gas bill
		double costUpTo6K = 2.35;	//used to assess price for cost up to 6k gallons
		double costUpTo20K = 3.75;	//used to assess price for cost over 6k up to 20k gallons
		double costOver20K = 6.00;	//used to assess price for cost over 20k gallons
		int fee = 15;		//$15 surchrage fee on all bills

		//get user input for gallons
		System.out.println("Enter the total number of gallons used, divided by 1000: ");
		//added prompt for clarification
		System.out.println("If 20,000 gallons used, enter 20");
		
		//call function to get user input
		gallons = final_utility_company_system.userDouble();
		
		//conditional for gas fee calculations
		//if gallons is over 20, x gallons over 20 will be calculated with costOver20k
		// && gallons between 6 and 20 will be calculated with costUpTo20k
		// && gallons under 6k will be calculated with costUpTo6k
		if (gallons > 20){
			charge = (gallons - 20) * costOver20K;
			charge = charge + (14 * costUpTo20K);
			charge = charge + (6 * costUpTo6K);
		//if gallons is under 20k, x gallons over 6k will be calculated with costUpTo20k
		// && gallons under 6k will calculated with costUpTo6k
		}else if (gallons > 6 && gallons <= 20)
		{
			charge = (gallons - 6) * costUpTo20K;
			charge = charge + (6 * costUpTo6K);
		//gallons under 6k will be calculated with costUpTo6k
		}else
		{
			charge = gallons * costUpTo6K;
		}

		//total will equal calculated charge plus $15 surcharge
		total = gasChargeCalc(fee, charge);
		
		//output total gallons used and the total bill amount
		System.out.println("You have used" + gallons + "thousand gallons of water");
		System.out.println("Your total water bill is $" + df.format(total));
	}

	//gasChargeCalc(int, double) takes two parameters from gasFeeCalculation()
	//and adds them together.  Used to add the fee to the total for water consumption
	private static double gasChargeCalc(int fee, double charge) 
	{
		double sum = 0;		//declare sum of fee and charge
		sum = fee + charge;	//add fee to charge
		return sum;			//return sum/total bill
	}
}
