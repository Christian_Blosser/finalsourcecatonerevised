/************************************************************************
 * Project: CS 405: Secure Coding Final Project/Utility Company System  *
 * Created: Nov. 5, 2017												*
 * Updated: 															*
 * 																		*
 * Original Author: Curtis												*
 * Updating Author: Christian Blosser									*
 * Description:															*
 * 		This program is a theoretical program that could be used at a	*
 * 		water treatment facility that supplies water to residents in	*
 * 		town.  It encompasses bill creation, bill calculation, and CRUD	*
 * 		operations for the billing system.  This program was originally	*
 * 		written in C++.  The following is program is Java.	For the most*
 * 		part, it was a line for line translation.  A few adjustments	*
 * 		were made due to some special requirements need for the final.	*
 ************************************************************************/

/*
 * File: 	final_utility_company_system.java
 * Created:	Nov. 5, 2017
 * Updated: Sep. 20, 2020
 * 
 * Author: 	Curtis
 * Updater: Christian Blosser
 * 
 * Description:
 * 		final_utility_company_system.java contains the main method as well
 * 		as all functions to read user input.
 */

package FinalProjectCode;
import java.util.Scanner;

public class final_utility_company_system 
{
	private static boolean Authentication;
	
	//create scanner instance object
	private static Scanner input = new Scanner(System.in);
	
	//Basic constructor method.  No values needed to input
	public void Final_Utility_Company_System()
	{
		//no variables need passed through constructor
	}
	
	/*
	 * menuPrint() simply displays the initial menu giving the user 
	 * options to choose from.
	 */
	public static void menuPrint()
	{
		System.out.println("MAIN MENU");
		System.out.println("01. CUSTOMER");
		System.out.println("02. ADMINISTRATOR");
		System.out.println("03. EXIT");
		System.out.println("Please Select Your Option (1-3)");
	}
	
	//userInteger() calls the scanner object for user input and
	//then returns the user input.  Input: int, output: int.
	public static int userInteger()
	{
		int num = 0;
		num = input.nextInt();
		input.nextLine();
		return num;
	}
	
	//userDouble() calls the scanner object for user input and
	//then returns the user input.  input: double, output double
	public static double userDouble()
	{
		double num = 0;
		num = input.nextDouble();
		input.nextLine();
		return num;
	}
	
	//userString calls the scanner object for user input and then
	//returns the user input.  input: string, output: string
	public static String userString()
	{
		String str;
		str = input.nextLine();
		return str;
	}
	
	/*
	 * The main method will initiate the program by prompting user input
	 * after a menu is displayed on screen.  The menu options will use the 
	 * Record object and call different methods or terminate program.
	 * @param args command line input will not be needed to pass variables
	 */
	public static void main(String[] args) throws Exception
	{
		int option = 0;	//keep track of user option
		Authentication = false;
		
		// do while loop to cycle through menu options, user input,
		// and switch statement.
		do
		{
			menuPrint();	//print menu
			option = userInteger();//get user input of int type
			
			//switch to case based on selection or fault
			switch (option) 
			{
				//case 1 accesses record.java and uses placeOrder()
				case 1:
					FinalProjectCode.record.placeOrder();
					break;
				//case 2 accesses record.java and uses adminMenu()
				case 2:
					//if user has previously logged on, skip authentication
					if(!Authentication)
					{
						login.loginRequest();
						Authentication = true;
					}
					//open administrative menu
					record.adminMenu();
					break;
				//case 3 terminates program
				case 3: 
					System.out.println("Goodbye.");
					input.close();
					System.exit(0);
					break;
				default: System.out.print("\b");
			}
		}while (option != 3);
		
		input.close();
	}
}
