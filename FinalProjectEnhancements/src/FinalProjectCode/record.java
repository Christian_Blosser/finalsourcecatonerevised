/************************************************************************
 * Project: CS 405: Secure Coding Final Project/Utility Company System  *
 * Created: Nov. 5, 2017												*
 * Updated: 															*
 * 																		*
 * Original Author: Curtis												*
 * Updating Author: Christian Blosser									*
 * Description:															*
 * 		This program is a theoretical program that could be used at a	*
 * 		water treatment facility that supplies water to residents in	*
 * 		town.  It encompasses bill creation, bill calculation, and CRUD	*
 * 		operations for the billing system.  This program was originally	*
 * 		written in C++.  The following is program is Java.	For the most*
 * 		part, it was a line for line translation.  A few adjustments	*
 * 		were made due to some special requirements need for the final.	*
 ************************************************************************/

/*
 * File: 	record.java
 * Created:	Nov. 5, 2017
 * Updated: Sep. 20, 2020
 * 
 * Author: 	Curtis
 * Updater: Christian Blosser
 * 
 * Description:
 * 		record.java handles all instances of record keeping.  It can display
 * 		the administrative menu, delete a service, create a service, modify
 * 		a service, display all or one service, or search for a service.  Each
 * 		service is stored in a CSV file and can be modified with use of this
 * 		program or through another CSV file editor.  Columns must be retained.
 */

package FinalProjectCode;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class record 
{	
	//file instance variable holds the name of the Services file name
	private static String fileName = "Services.csv";
	
	public static void adminMenu() throws IOException
	{
		int adminOption = 0; //keep track of user option
		int adminOptionTwo = 0;	//keep track of second user option for switch case
			
		//print admin menu of 8 options
		System.out.println("\n\tADMIN MENU");
		System.out.println("1.CREATE SERVICE");
		System.out.println("2.DISPLAY ALL SERVICES");
		System.out.println("3.QUERY ");
		System.out.println("4.GAS SERVICE FEE ");
		System.out.println("5.MODIFY SERVICE");
		System.out.println("6.DELETE SERVICE");
		System.out.println("7.VIEW SERVICE MENU");
		System.out.println("8.BACK TO MAIN MENU");
		System.out.println("Please Enter Your Choice (1-8) \n");
		
		//get user input for adminOption
		adminOption = final_utility_company_system.userInteger();
		
		switch (adminOption) {
		//case 1 takes user to writeService()
		case 1:
			writeService();
			break;
		//case 2 displays all services
		case 2:
			displayAll();
			break;
		//case 3 queries all services
		case 3:
			System.out.println("Please Enter The Service Number");
			//get user input of type int
			adminOptionTwo = final_utility_company_system.userInteger();
			//display service of service number entered
			displayService(adminOptionTwo);
			break;
		//case 4 takes user to gas.java and uses gasFeeCalculation()
		case 4:
			FinalProjectCode.gas.gasFeeCalculation();
			break;
		//case 5 uses modifyService()
		case 5:
			modifyService();
			break;
		//case 6 uses deleteService()
		case 6:
			//prompt user for input
			System.out.println("Delete Record\nPlease Enter the Service Number of the\n"
					+ "Service You Want To Delete");
			int servNum = final_utility_company_system.userInteger();	//input
			//call delete service()
			deleteService(servNum);
			break;
		//case 7 uses serviceMenu()
		case 7:
			System.out.println("Removed option due to lost file");
			break;
		//case 8 takes user to final_utility_company_system and uses menuPrint()
		case 8:
			//auto return to main method
			break;
		default:
			System.out.println("\b");
			adminMenu();
			break;
		}
	}
	
	//deleteService(int) handles the deletion of a service.  it first creates
	//a temp file, reads Services.csv, and translates each service over except
	//for the record that is to be deleted.
	//@param servNum the number of the service to be deleted
	private static void deleteService(int servNum) throws IOException 
	{
		//open two files, one to keep, one to delete
		File fileToDelete = new File(fileName);
		File fileToKeep = new File("temp.csv");
		
		//open Scanner and FileWriter to copy one from the other
		Scanner copier = new Scanner(fileToDelete);
		FileWriter copied = new FileWriter(fileToKeep, true);
		
		//designate delimiter for csv
		copier.useDelimiter(",");
		
		//array to hold service to delete
		String[] servToDelete = findService(servNum);
		
		//while entries still exist in copier
		while(copier.hasNext())
		{
			//temp is the next entry
			String temp = copier.next();
			//if temp equals the service number
			if (servToDelete[0].equals(temp) || servToDelete[0].equals("\n"+temp))
			{
				//skip the next 3 entries
				copier.next();
				copier.next();
				copier.next();
			}else if (!temp.equals("\n"))	//otherwise
			{
				//copy them to the new temp file
				copied.append(temp+",");
			}
		}
		//add new line for new entry
		copied.append("\n");
		
		//close open scanner and writer
		copied.flush();
		copier.close();
		copied.close();
		
		//remove original file
		if (fileToDelete.delete())
		{
			//confirm removal and rename temp to Services
			System.out.println("Record Deleted");
			fileToKeep.renameTo(fileToDelete);
		}
	}
	
	//modifyService() get user input for service number to be modified
	//and then deletes the service and creates a new one.
	private static void modifyService() throws IOException 
	{
		//keep track of the service number to modify
		int servNum = 0;
		
		//display prompt for user
		System.out.println("To Modify, Please Enter the:\nService Number of the Service");
		servNum = final_utility_company_system.userInteger();	//input
		
		//delete entry and then create a new one
		deleteService(servNum);
		
		writeService();
	}
	
	//displayService(int) calls findService() to find a specific service
	//It then displays what is returned from findService.
	private static void displayService(int adminOptionTwo) throws FileNotFoundException
	{
		//Array to hold found service and int to hold length of service
		String[] service = findService(adminOptionTwo);
		int serviceArrLen = service.length;
		
		//print display header
		System.out.println("Number\tName\tPrice\tDiscount");
		for (int i = 0; i < serviceArrLen; i++)
		{
			System.out.print(service[i]+"\t");
		}
		System.out.println();
	}
	
	//findService(int) opens the Services.csv file and searches for
	//the service number entered by user.
	private static String[] findService(int adminOptionTwo) throws FileNotFoundException 
	{
		//length of all service entries
		int serviceLen = 4;
		int counter = 0; //array storage control
		//array for service to be displayed
		String[] service = new String[serviceLen];
		
		//open file to read
		Scanner displayCSV = new Scanner(new File(fileName));
		displayCSV.useDelimiter(",");	//read comma to separate columns
		
		//loop through file to find number of entries
		while (displayCSV.hasNext())	//while more exists
		{
			//temp variable to hold next
			String temp = displayCSV.next();
			
			if (String.valueOf(adminOptionTwo).equals(temp) || String.valueOf("\n"+adminOptionTwo).equals(temp))
			{
				service[counter] = temp;//store customer number
				counter++;	//increment for next open array spot
				service[counter] = displayCSV.next();
				counter++;	//increment for next open array spot
				service[counter] = displayCSV.next();
				counter++;	//increment for next open array spot
				service[counter] = displayCSV.next();
				break;
			}
		}
		//close scanner object
		displayCSV.close();
		
		//return String[] array
		return service;
	}
	
	//displayAll() scans the Services.csv file and displays all of the entires
	//on screen for user to see
	private static void displayAll() throws FileNotFoundException 
	{
		//display prompt
		System.out.println("DISPLAY ALL RECORDS");
		
		//open file to read
		Scanner displayCSV = new Scanner(new File(fileName));
		displayCSV.useDelimiter(",");	//read comma to separate columns
		
		//loop through file to find number of entries
		while (displayCSV.hasNext())	//while more exists
		{
			System.out.print(displayCSV.next() + "\t");	//go to next entry
		}
		
		//close scanner
		displayCSV.close();
		
	}
	
	//writeService() is used to handle service creation.  This method is 
	//called if no Services.csv file exists or if a new service needs
	//added.  It calls createService from service.java and enters each
	//variable into the Services.csv file.  If it is first-time write,
	//the csv column headers will be added
	private static void writeService() throws IOException 
	{		
		//call create service to prompt user to create new service
		service.createService();
		
		File serviceFile = new File(fileName);
		
		//open file writer object to write to CSV Services file
		FileWriter writeServiceFile = new FileWriter(serviceFile, true);
		
		//if file is empty
		if (serviceFile.length() == 0)
		{
			//write csv column headers, append to empty file
			writeServiceFile.append("Number,");
			writeServiceFile.append("Name,");
			writeServiceFile.append("Price,");
			writeServiceFile.append("Discount,");
			//add new line for user friendly viewing csv file
			writeServiceFile.append("\n");
		}
		
		//append the service information
		//convert ints and doubles to strings and append to file
		writeServiceFile.append(String.valueOf(service.getCusNum())+",");
		writeServiceFile.append(service.getServName()+",");
		writeServiceFile.append(String.valueOf(service.getServPrice())+",");
		writeServiceFile.append(String.valueOf(service.getDiscount())+",");
		//add new line for user friendly viewing csv file
		writeServiceFile.append("\n");
		
		//close FileWriter
		writeServiceFile.close();
	}
	
	
	public static void placeOrder() {
		// TODO Auto-generated method stub
		System.out.println("This part of the program has not been recreated");
		
	}
}
